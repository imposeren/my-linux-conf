#!/bin/sh
sudo -u imposeren -s -- <<EOF
    export DISPLAY=:0
    export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
    export XAUTHORITY=/home/imposeren/.Xauthority
    /usr/bin/notify-send -u critical -a clamav -i /usr/share/app-install/icons/clamtk.png "$1" "$2"
    /usr/bin/paplay /usr/share/sounds/freedesktop/stereo/window-attention.oga
    echo "\n\n" >>  /home/imposeren/.notes.tail
    echo "$1" >>  /home/imposeren/.notes.tail
    echo "$2" >> /home/imposeren/.notes.tail
EOF

