#!/bin/bash

export _JAVA_OPTIONS='-Dsun.java2d.uiScale.enabled=false -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

# Some apps are beter with Nimbus look-n-feel
# export _JAVA_OPTIONS='-Dsun.java2d.uiScale=2 -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=javax.swing.plaf.nimbus.NimbusLookAndFeel  -Dswing.crossplatformlaf=javax.swing.plaf.nimbus.NimbusLookAndFeel'
