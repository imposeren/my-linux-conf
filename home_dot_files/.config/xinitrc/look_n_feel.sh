#!/bin/bash

OLD_IFS="$IFS"
DYNAMIC_XRESOURCES_PATH="$1"
DRY_RUN=$2

#WIDGETS_THEME="Breeze-Dark"
LXQT_WIDGETS_THEME="qt5ct-style"
ICON_THEME="Breeze Dark"
#QT5_CT_COLOR_CONF="${HOME}/.config/qt5ct/colors/dark.conf"
FONT_ANTIALIAS="true"
FONT_AUTOHINT="false"
FONT_HINTING="true"
FONT_HINTSTYLE="hintslight"
FONT_LCDFILTER="lcddefault"
FONT_RGBA="rgb"
FONT_NAME_FIXED="Hack"
FONT_NAME_MAIN="Noto Sans"
FONT_SIZE_MAIN=10
FONT_SIZE_FIXED=9

if [ "${FONT_ANTIALIAS}" = "true" ] ; then
    FONT_ANTIALIAS_INT=1
else
    FONT_ANTIALIAS_INT=0
fi

if [ "${FONT_AUTOHINT}" = "true" ] ; then
    FONT_AUTOHINT_INT=1
else
    FONT_AUTOHINT_INT=0
fi

if [ "${FONT_HINTING}" = "true" ] ; then
    FONT_HINTING_INT=1
else
    FONT_HINTING_INT=0
fi

if [ "${IS_MAIN_SCREEN_HIDPI}" -eq 1 ] ; then
    CURSOR_THEME="Chameleon-White-Large"
    CURSOR_SIZE=48
    SCALING_FACTOR=2
else
    CURSOR_THEME="Chameleon-White-Regular"
    CURSOR_SIZE=24
    SCALING_FACTOR=1
fi

function hex_to_triplet() {
    NORMALIZED=`echo $1 | tr -d '#' | tr /a-z/ /A-Z/ `
    RED=`echo "ibase=16; ${NORMALIZED:0:2}" | bc`
    GREEN=`echo "ibase=16; ${NORMALIZED:2:2}" | bc`
    BLUE=`echo "ibase=16; ${NORMALIZED:4:2}" | bc`
    echo "${RED},${GREEN},${BLUE}"
}

function dgconf() {
    if [ -n "$4" ] ; then
        dconf_type=""
        if [ "$4" = "int" ] ; then
            dconf_type="uint32"
        fi

        if [ -n "$5" ] ; then
            if [ "$DRY_RUN" -eq 1 ] ; then
                echo gconftool --type=$4 --list-type=$5 -s /desktop/gnome/$1/${2//-/_} "$3"
            else
                gconftool --type=$4 --list-type=$5 -s /desktop/gnome/$1/${2//-/_} "$3"
            fi
        else
            if [ "$DRY_RUN" -eq 1 ] ; then
                echo gconftool --type=$4 -s /desktop/gnome/$1/${2//-/_} "$3"
            else
                gconftool --type=$4 -s /desktop/gnome/$1/${2//-/_} "$3"
            fi
        fi
    fi

    if [ "$DRY_RUN" -eq 1 ] ; then
        echo gsettings set org.gnome.desktop.${1//-/.} $2 $3 '||' dconf write /org/gnome/desktop/$1/$2 "${dconf_type} $3"
    else
	set -x
        if ! gsettings set org.gnome.desktop.${1//-/.} $2 $3 2>>/dev/null ; then
            dconf write /org/gnome/desktop/$1/$2 "${dconf_type} $3"
        fi
	set +x
    fi
}

if [ -f "${QT5_CT_COLOR_CONF}" ] ; then
    echo "Using qt5ct as source for color scheme"
    _IND_FG_WINDOW=0
    _IND_BG_BUTTON=1
    _IND_BG_BRIGHT=2
    _IND_BG_LESS_BRIGHT=3
    _IND_BG_DARK=4
    _IND_BG_LESS_DARK=5
    _IND_FG_COMMON=6
    _IND_FG_BRIGHT=7
    _IND_FG_BUTTON=8
    _IND_BG_COMMON=9
    _IND_BG_WINDOW=10
    _IND_SHADOW=11
    _IND_BG_SELECTED=12
    _IND_FG_SELECTED=13
    _IND_LINK=14
    _IND_VISITED_LINK=14
    _IND_BG_ALT=16
    _IND_FG_DEFAULT=17
    _IND_BG_TIP=18
    _IND_FG_TIP=19
    ACTIVE_COLORS=`grep -E '^active_colors' "${QT5_CT_COLOR_CONF}" | cut -d= -f2 | cut -d, -f 1-`
    INACTIVE_COLORS=`grep -E '^inactive_colors' "${QT5_CT_COLOR_CONF}" | cut -d= -f2 | cut -d, -f 1-`
    DISABLED_COLORS=`grep -E '^disabled_colors' "${QT5_CT_COLOR_CONF}" | cut -d= -f2 | cut -d, -f 1-`
    IFS=', ' read -r -a ACTIVE_COLORS <<< "${ACTIVE_COLORS}"
    IFS=', ' read -r -a INACTIVE_COLORS <<< "${INACTIVE_COLORS}"
    IFS=', ' read -r -a DISABLED_COLORS <<< "${DISABLED_COLORS}"
    IFS="$OLD_IFS"

    GTK_COLORS_CONFIG="'base_color:${INACTIVE_COLORS[$_IND_BG_WINDOW]};bg_color:${INACTIVE_COLORS[$_IND_BG_WINDOW]};text_color:${INACTIVE_COLORS[$_IND_FG_DEFAULT]};fg_color:${INACTIVE_COLORS[$_IND_FG_DEFAULT]};selected_bg_color:${INACTIVE_COLORS[$_IND_BG_SELECTED]};selected_fg_color:${INACTIVE_COLORS[$_IND_FG_SELECTED]};tooltip_bg_color:${INACTIVE_COLORS[$_IND_BG_TIP]};tooltip_fg_color:${INACTIVE_COLORS[$_IND_FG_TIP]};panel_bg_color:${INACTIVE_COLORS[$_IND_BG_WINDOW]};panel_fg_color:${INACTIVE_COLORS[$_IND_FG_BUTTON]};menu_bg_color:${INACTIVE_COLORS[$_IND_BG_WINDOW]};menu_fg_color:${INACTIVE_COLORS[$_IND_FG_BUTTON]}'"

    # set gtk colors using dconf:
    dgconf background primary-color "'${ACTIVE_COLORS[$_IND_BG_WINDOW]}'" string
    dgconf interface gtk-color-scheme "${GTK_COLORS_CONFIG}" string
    
    echo "[Colors:Button]
BackgroundNormal=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_BG_BUTTON]})
DecorationFocus=$(hex_to_triplet ${DISABLED_COLORS[$_IND_BG_BUTTON]})
DecorationHover=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_BG_BUTTON]})
ForegroundInactive=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_FG_BUTTON]})
ForegroundNormal=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_FG_BUTTON]})

[Colors:Tooltip]
BackgroundNormal=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_BG_TIP]})
ForegroundNormal=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_FG_TIP]})

[Colors:Window]
BackgroundNormal=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_BG_WINDOW]})
ForegroundNormal=$(hex_to_triplet ${ACTIVE_COLORS[$_IND_FG_WINDOW]})
ForegroundInactive=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_FG_WINDOW]})

[General]
font=${FONT_NAME_MAIN},${FONT_SIZE_MAIN},-1,5,50,0,0,0,0,0
menuFont=${FONT_NAME_MAIN},${FONT_SIZE_MAIN},-1,5,50,0,0,0,0,0
toolBarFont=${FONT_NAME_MAIN},$(echo ${FONT_SIZE_MAIN}-2|bc),-1,5,50,0,0,0,0,0
shadeSortColumn=false
background=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_BG_COMMON]})
foreground=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_FG_COMMON]})
linkColor=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_LINK]})
buttonBackground=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_BG_BUTTON]})
buttonForeground=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_FG_BUTTON]})
windowBackground=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_BG_WINDOW]})
windowForeground=$(hex_to_triplet ${INACTIVE_COLORS[$_IND_FG_WINDOW]})

[Icons]
Theme=${ICON_THEME}
" | crudini --merge "${HOME}/.kde/share/config/kdeglobals"

fi


> "${DYNAMIC_XRESOURCES_PATH}" echo "
*TkTheme: ${WIDGETS_THEME}
Xft.dpi: ${MAIN_DPI}
Xft.antialias: ${FONT_ANTIALIAS}
Xft.hinting: ${FONT_HINTING}
Xft.hintstyle: ${FONT_HINTSTYLE}
Xft.autohint: ${FONT_AUTOHINT}
Xft.lcdfilter:${FONT_LCDFILTER}
Xft.rgba:${FONT_RGBA}

Xcursor.theme: ${CURSOR_THEME}
Xcursor.size: ${CURSOR_SIZE}

URxvt.font: xft:${FONT_NAME_FIXED}:style=Regular:size=${FONT_SIZE_FIXED}
URxvt.boldFont: xft:${FONT_NAME_FIXED}:style=Bold:size=${FONT_SIZE_FIXED}
XTerm*faceName: ${FONT_NAME_FIXED}
"

if false ; then
    # update gtk-3.0 settings
    echo "[Settings]
gtk-theme-name=${WIDGETS_THEME}
gtk-icon-theme-name=${ICON_THEME}
gtk-font-name=${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}
gtk-cursor-theme-name=${CURSOR_THEME}
gtk-cursor-theme-size=${CURSOR_SIZE}
gtk-xft-antialias=${FONT_ANTIALIAS_INT}
gtk-xft-hinting=${FONT_HINTING_INT}
gtk-xft-hintstyle=${FONT_HINTSTYLE}
gtk-xft-rgba=${FONT_RGBA}
" | crudini --merge "${HOME}/.config/gtk-3.0/settings.ini"

# update .gtkrc and .gtkrc-2.0
GTK_RC_CONTENT="include \"/usr/share/themes/${WIDGETS_THEME}/gtk-2.0/gtkrc\"

style \"user-font\" {
        font_name = \"${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}\"
}

widget_class \"*\" style \"user-font\"

gtk-font-name=\"${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}\"
gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ

include \"${HOME}/.gtkrc.mine\"
"

GTK_RC2_CONTENT="# DO NOT EDIT! This file will be overwritten by LXAppearance.
# Any customization should be done in ~/.gtkrc-2.0.mine instead.

include \"/usr/share/themes/${WIDGETS_THEME}/gtk-2.0/gtkrc\"

style \"user-font\" {
        font_name = \"${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}\"
}

widget_class \"*\" style \"user-font\"

gtk-theme-name=\"${WIDGETS_THEME}\"
gtk-icon-theme-name=\"${ICON_THEME}\"
gtk-font-name=\"${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}\"
gtk-cursor-theme-name=\"${CURSOR_THEME}\"
gtk-cursor-theme-size=${CURSOR_SIZE}
gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ
gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
gtk-button-images=1
gtk-menu-images=1
gtk-enable-event-sounds=1
gtk-enable-input-feedback-sounds=0
gtk-xft-antialias=${FONT_ANTIALIAS_INT}
gtk-xft-hinting=${FONT_HINTING_INT}
gtk-xft-hintstyle=\"${FONT_HINTSTYLE}\"
gtk-xft-rgba=\"${FONT_RGBA}\"
gtk-modules=\"canberra-gtk-module\"

include \"/home/imposeren/.gtkrc-2.0.mine\"
"

    echo "${GTK_RC_CONTENT}" > ${HOME}/.gtkrc
    echo "${GTK_RC2_CONTENT}" > ${HOME}/.gtkrc-2.0


# update lxqt settings
    echo "[General]
icon_theme=${ICON_THEME}
single_click_activate=false
theme=ambiance

[Qt]
font=\"${FONT_NAME_MAIN},${FONT_SIZE_MAIN},-1,5,50,0,0,0,0,0\"
style=${LXQT_WIDGETS_THEME#:$WIDGETS_THEME}
" | crudini --merge "${HOME}/.config/lxqt/lxqt.conf"

    echo "[Mouse]
cursor_theme=${CURSOR_THEME}
" | crudini --merge "${HOME}/.config/lxqt/session.conf"



    # update dconf settings
    dgconf interface gtk-key-theme "'${WIDGETS_THEME}'" string
    dgconf interface gtk-theme "'${WIDGETS_THEME}'" string

    dgconf interface scaling-factor ${SCALING_FACTOR} int
    dgconf interface text-scaling-factor 1 int

    dgconf interface cursor-theme "'${CURSOR_THEME}'" string

    dgconf interface icon-theme "'${ICON_THEME}'" string

    dgconf interface document-font-name "'${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}'"
    dgconf interface font-name "'${FONT_NAME_MAIN} ${FONT_SIZE_MAIN}'"
    dgconf interface monospace-font-name "'${FONT_NAME_FIXED} ${FONT_SIZE_FIXED}'"

fi

if [ "$DRY_RUN" -eq 1 ] ; then
    set -v
    set -n
fi

xrdb -override "${DYNAMIC_XRESOURCES_PATH}"
