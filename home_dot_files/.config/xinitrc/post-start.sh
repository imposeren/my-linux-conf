#!/bin/bash

OLD_IFS="$IFS"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function autostart_programs() {
    #autocutsel -selection PRIMARY -fork &
    urxvt -name htop -e htop &
#    xxkb &
#    xsettingsd &
#    numlockx &
#    xbindkeys &
#    nvidia-settings -l
#    compton -b
    SAL_USE_VCLPLUGIN=gtk3 spacefm &
    yeahconsole -e $HOME/bin/tmux-rds yeah &
}


function delayed_autostart_programs() {
    sleep 1
    /usr/bin/google-chrome &
    sleep 3
    /opt/sublime_text/sublime_text &
    urxvt -cd /home/imposeren/work/tika-dot-ai/ -name sublime-dev -e $HOME/bin/tmux-rds sublime-dev &
    skypeforlinux &
    /home/imposeren/bin/telegram-desktop &
    sleep 5
#     libreoffice --quickstart &
    jscal-restore /dev/input/js0
}

autostart_programs
delayed_autostart_programs &
exit 0
