#!/bin/bash

OLD_IFS="$IFS"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HIGH_DPI_MIN=130

NORMAL_DPI_SCRIPT_PATH="${SCRIPT_DIR}/normal_dpi.sh"
HIGH_DPI_SCRIPT_PATH="${SCRIPT_DIR}/high_dpi.sh"
LAPTOP_SCRIPT_PATH="${SCRIPT_DIR}/laptop.sh"
DESKTOP_SCRIPT_PATH="${SCRIPT_DIR}/desktop.sh"
DYNAMIC_XRESOURCES_PATH="${SCRIPT_DIR}/Xresources-dynamic"

TEST_RUN=0
DRY_RUN=0

while getopts 'td' arg
    do
        case ${arg} in
            d) DRY_RUN=1;;
            t) TEST_RUN=1;;
            *) exit 1 # illegal option
        esac
    done

function seat_properties() {
    MAIN_DPI=`xdpyinfo | grep resolution | grep -oE -e '[0-9]+' | head -n1`
    if [ "${MAIN_DPI}" -eq "187" ] ; then
	MAIN_DPI=192
    fi

    if [ "${MAIN_DPI}" -ge "${HIGH_DPI_MIN}" ] ; then
        IS_MAIN_SCREEN_HIDPI=1
    else
        IS_MAIN_SCREEN_HIDPI=0
    fi

    if hostname -s | grep -q laptop ; then
        IS_LAPTOP=1
    else
        IS_LAPTOP=0
    fi

    read -r -a XRANDR_OUTPUTS <<< $(xrandr --current -q | grep -w connected | cut -d' ' -f 1)

    export MAIN_DPI
    export IS_MAIN_SCREEN_HIDPI
    export IS_LAPTOP
    export XRANDR_OUTPUTS
}

function common_init() {
    # actions common for hidpi and normal dpi
    # configure and export env variables
    export SAL_USE_VCLPLUGIN=gen
    export TCLLIBPATH=~/.local/share/tkthemes
#    export QT_PLATFORMTHEME=qt5ct
#    export QT_PLATFORM_PLUGIN=qt5ct
#    export QT_QPA_PLATFORMTHEME=qt5ct
#    export QT_AUTO_SCREEN_SCALE_FACTOR=1
    export XDG_DATA_HOME=/home/imposeren/.local/share/
    export XDG_CACHE_HOME=${HOME}/.cache
    export ICEAUTHORITY=${XDG_RUNTIME_DIR}/ICEauthority
#    export GTK_THEME=Breeze-Dark
    export IS_MAIN_SCREEN_HIDPI
    export IS_LAPTOP

    mkdir -p $XDG_CACHE_HOME 2>>/dev/null || true

#    wmname LG3D
    xrdb -override ${SCRIPT_DIR}/Xresources-common
    xrdb -override ${SCRIPT_DIR}/Xresources-monokai
    xrandr --dpi ${MAIN_DPI}

    echo "export DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS" > ~/.Xdbus
}


function normal_init() {
    if [ -x "${NORMAL_DPI_SCRIPT_PATH}" ] ; then
        . "${NORMAL_DPI_SCRIPT_PATH}"
    fi

    xset m 2 0
}

function hidpi_init() {
    if [ -x "${HIGH_DPI_SCRIPT_PATH}" ] ; then
        . "${HIGH_DPI_SCRIPT_PATH}"
    fi

    # # scaling factor is already set in look_n_feel using gsettings/gconftool -> do not use gdk scaling
    # export GDK_SCALE=1
    # export GDK_DPI_SCALE=1

    xrdb -override ${SCRIPT_DIR}/Xresources-hidpi

    xset m 2 3

}

function laptop_init() {
    if [ -x "${LAPTOP_SCRIPT_PATH}" ] ; then
        . "${LAPTOP_SCRIPT_PATH}"
    fi

    xrdb -override "${SCRIPT_DIR}/Xresources-laptop"
    xset m 2 0

    if [[ " ${XRANDR_OUTPUTS[@]} " =~ " eDP1 " ]] ; then
        xrandr --output eDP1 --gamma 0.9:0.8:0.7
    fi
    if [[ " ${XRANDR_OUTPUTS[@]} " =~ " DP-1 " ]] ; then
        xrandr --output HDMI --gamma 0.75:0.75:0.75
    fi

}

function desktop_init() {
    if [ -x "${DESKTOP_SCRIPT_PATH}" ] ; then
        . "${DESKTOP_SCRIPT_PATH}"
    fi

    if [[ " ${XRANDR_OUTPUTS[@]} " =~ " DP-1 " ]] ; then
        xrandr --output DP-1 --gamma 0.95:0.95:0.95
    fi
}

function keyboard_layout() {
    LAYOUT="us,ru,ua"
    MODEL="pc105"
    OPT1=compose:ralt
    OPT2=grp:caps_toggle
    OPT3=grp_led:scroll
    OPT4=misc:typo
    OPT5=lv3:menu_switch
    OPT6=terminate:ctrl_alt_bksp
    VARIANT=",,"

    setxkbmap -option ${OPT1},${OPT2},${OPT3},${OPT4},${OPT5},${OPT6} -model ${MODEL} -layout ${LAYOUT} -variant ${VARIANT}

#    dconf write /org/gnome/desktop/input-sources/sources "[('xkb', 'us'), ('xkb', 'ru'), ('xkb', 'ua')]"
#    dconf write /org/gnome/desktop/input-sources/xkb-options "['${OPT1}', '${OPT2}', '${OPT3}', '${OPT4}', '${OPT5}', '${OPT6}']"

echo "[Keyboard]
layout=\"${LAYOUT}\"
model=${MODEL}
numlock=true
options=compose:ralt, grp_led:scroll, grp:caps_toggle
variant=\"${VARIANT}\"
" | crudini --merge "${HOME}/.config/lxqt/session.conf"


}

seat_properties
if [ -f ${SCRIPT_DIR}/env-overrides ] ; then
    . ${SCRIPT_DIR}/env-overrides
fi
common_init
keyboard_layout


if [ "${IS_MAIN_SCREEN_HIDPI}" -eq 1 ] ; then
    hidpi_init
else
    normal_init
fi


if [ "${IS_LAPTOP}" -eq 1 ] ; then
    laptop_init
else
    desktop_init
fi

${SCRIPT_DIR}/look_n_feel.sh "${DYNAMIC_XRESOURCES_PATH}" ${DRY_RUN}


if [ "$TEST_RUN" -eq 1 ] || [ "$DRY_RUN" -eq 1 ] ; then
    set -v
    set -n
fi

${SCRIPT_DIR}/post-start.sh
