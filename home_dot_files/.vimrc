
call plug#begin('~/.vim/bundle')

Plug 'bitc/vim-bad-whitespace'
Plug 'crusoexia/vim-monokai'
Plug 'vim-scripts/Vimchant'

call plug#end()

let python_highlight_all=1

set encoding=utf-8
set autoindent
set pastetoggle=<F7>

set foldmethod=indent
set shiftwidth=4

let g:vimpager = {}                                        
let g:less     = {}                                        
let g:less.enabled = 0                                     
let g:vimpager.passthrough = 0                             
let g:vimpager.X11 = 0 

au BufRead,BufNewFile *.py,*pyw,*.js,*.css,*.less set shiftwidth=4
au BufRead *.c,*.h set shiftwidth=4
au BufNewFile *.c,*.h set shiftwidth=4

au BufRead,BufNewFile *py,*pyw,*.c,*.h,*.js,*.css,*.less set tabstop=4

au BufRead,BufNewFile *.py,*.pyw,*.js,*.css,*.less set expandtab
au BufRead,BufNewFile *.c,*.h set noexpandtab
au BufRead,BufNewFile Makefile* set noexpandtab

au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=79
au BufRead,BufNewFile *.txt set textwidth=999999

au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r

au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

autocmd BufRead *.py set makeprg=python\ -c\ \"import\ py_compile,sys;\ sys.stderr=sys.stdout;\ py_compile.compile(r'%')\"
autocmd BufRead *.py set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
autocmd BufRead *.py nmap <F9> :!python %<CR>
noremap <silent> <F11> :cal VimCommanderToggle()<CR>

let g:fencview_autodetect = 1

set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

set updatetime=1000
set gfn=Droid\ Sans\ Mono


highlight DiffAdd term=reverse cterm=bold ctermbg=green ctermfg=white
highlight DiffChange term=reverse cterm=bold ctermbg=cyan ctermfg=black
highlight DiffText term=reverse cterm=bold ctermbg=blue ctermfg=white
highlight DiffDelete term=reverse cterm=bold ctermbg=red ctermfg=black 
