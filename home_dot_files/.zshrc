# It should contain cmmands to set up aliases, functions, options, key bindings, etc.

################################### locale ######################################
[[ $- != *i* ]] && return
[ -z "$PS1" ] && return
clear
if [ -f ~/.notes.head ]; then
    cat ~/.notes.head
fi
if [ -f ~/.notes.tail ]; then
    cat ~/.notes.tail
fi

source /etc/zsh/zprofile
export TEXMFLOCAL=${HOME}/texmf
export PATH="/home/${USER}/bin:${PATH}:${HOME}/.local/bin:/opt/sublime_text/"
export PAGER='vimpager'
export MANPAGER="vimpager"
#export MANPAGER="less"
#

# ohmyzsh config start
export ZSH=$HOME/.oh-my-zsh
#ZSH_THEME="robbyrussell"
ZSH_THEME="af-magic"
DISABLE_AUTO_UPDATE="true"
DISABLE_LS_COLORS="true"
TRUSTED_OH_MY_REVISION="d646884add277d134235a9b18ab755388d6e0d8d"

if [ $UID = 0 ] ; then
    ZSH_DISABLE_COMPFIX="true"
    plugins=(
    	command-not-found
	man
	sudo
#	themes
    )
else
    plugins=(
        command-not-found
	copyfile
	docker-compose
    	git
	git-escape-magic
	gitfast
	gpg-agent
	keychain
	man
	ssh-agent
	sudo
#	themes
	virtualenvwrapper
    )
fi

zstyle :omz:plugins:keychain options --quiet --nogui --timeout 120
zstyle :omz:plugins:keychain agents gpg,ssh


# ohmyzsh config end

export GPG_TTY=$(tty)

#java
#################################### load modules ####################################
autoload -U compinit
compinit

#################################### aliases ####################################
pycalc(){
	ipython -i -c "
from __future__ import division
from __future__ import print_function
from math import *
import datetime
print($1)
"}
lia(){alias | grep $*}
#alias em="pmerge --with-built-depends --ask"
#alias pmerge="pmerge --with-built-depends --ask"
#alias pmerge="pmerge --ask"
#alias lay="layman"
alias pwgen="pwgen -Bcn"
alias pwgens="pwgen -Bcny"
alias scr="screen"
alias sudo="sudo -E"
alias nsdet="netstat -tnup"
alias nsall="netstat -tnupa"
alias gcpn="git cherry-pick --no-gpg-sign"

# alias worldup="sudo emerge -a --update --deep --newuse --keep-going --ignore-default-opts @world && revdep-rebuild"
# alias eminst="sudo emerge --ignore-default-opts"
# alias emergebin="emerge-1a --binpkg-respect-use n -gK"
alias screens="screen -adRRO"

# alias x="startx"

alias grep='egrep --colour=auto'
alias greppy='egrep --colour=auto --include="*.py"'
alias ls='ls --group-directories-first --color=auto'
alias la='ls -la'
alias timidity='nice -15 timidity -a'

alias nmapa='nmap -P0 -sT -O -A'
alias nmapf='nmap -F'
alias df='df -H'

alias mkdir="nocorrect mkdir"

alias gchmaster='git checkout master; git pull'
alias rst2pdf='rst2pdf --stylesheets dejavu,eightpoint'

projmux() {
    PROJ=$(basename `pwd`);
    tmux -2 new-session -s proj-$PROJ || tmux -2 new-session -t proj-$PROJ
}

alias pmux='projmux'
alias encheck='enchant -L -a'

alias gcurr="git branch|grep '\*'|cut -d ' ' -f2"
alias gdiff='git diff --no-ext-diff'
alias gmerge='test `gcurr` = "master" && ZGIT_EXTRA=( "--no-ff" "" ) ;  git merge $ZGIT_EXTRA --edit'
# NOTE; ohmyzsh git plugin is loaded and also adds a lot of aliases

dcrunning(){
  docker-compose ps --filter status=running --format json | jq -s -e ".[] | select( .Service == \"$1\")" >> /dev/null
}

compctl -K _gmerge_complete gmerge
#compdef $_comps[git] gmerge

autoload -U zsh-mime-setup
zsh-mime-setup

alias -s exe=wine
#alias -s {jpg,png,bmp,gif}=viewnior
#alias -s svg=inkscape
#alias -s {avi,mpeg,mpg,mov,m2v,flv,mp4,mkv}=smplayer
#alias -s {odt,doc,sxw,rtf,xls}=libreoffice
alias -s {ogg,mp3,wav,wma}=mpg123
#alias -s pdf=acroread
#alias -s {html,htm}=sensible-browser

#################################### load modules ####################################

autoload -U promptinit
promptinit

autoload -U zfinit
zfinit

autoload -U incremental-complete-word
zle -N incremental-complete-word

autoload -U predict-on
zle -N predict-on
#zle -N predict-off

autoload -U zcalc

autoload -U colors
colors

# Autoload zsh modules when they are referenced
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
zmodload -ap zsh/mapfile mapfile

autoload -U insert-files
zle -N insert-files

autoload -U pick-web-browser


#################################### history ####################################
setopt  APPEND_HISTORY
setopt  INC_APPEND_HISTORY
setopt  SHARE_HISTORY
setopt  HIST_IGNORE_ALL_DUPS
setopt  HIST_IGNORE_SPACE
setopt  HIST_REDUCE_BLANKS
setopt  HIST_VERIFY
setopt  HIST_EXPIRE_DUPS_FIRST


FLEX_HOME=/home/imp/kava/misk/flexsdk

HISTFILE=~/.zhistory
HISTSIZE=1000000
SAVEHIST=1000000

#################################### prompt ####################################
PROMPT="%B%(!.%F{yellow}.%F{cyan})%n@%m%f %F{blue}%~%f %F{green}%(!.#.$)%f%b%{$reset_color%} "
RPROMPT="%F{red}< %f%F{cyan}%B%*%b%f" # "$yellow%y%b$white"

#################################### watch for login/logout events ################
watch=(all)
LOGCHECK=180
WATCHFMT='%n %a %l from %M at %T.'


#################################### env ####################################
# Some environment variables
#export MAIL=/var/spool/mail/$USERNAME
#export LESS=-cex3M
#export manpath=($X11HOME/man /usr/man /usr/lang/man /usr/local/man /usr/share/man)
export HELPDIR=/usr/local/lib/zsh/help  # directory for run-help function to find docs

export EDITOR="vim"
export GIT_EDITOR=$EDITOR
export VISUAL="view"

# automatically remove duplicates from these arrays
typeset -U path cdpath fpath manpath

# Hosts to use for completion (see later zstyle)
hosts=(`hostname` tohiba chipa asher komintern hosting5 mrsaint noc f ya.ru google.com ftp.ntu-kpi.kiev.ua vortex acts)

# Autoload all shell functions from all directories in $fpath (following symlinks) that have the executable bit on (the executable bit is not
# necessary, but gives you an easy way to stop the autoloading of a particular shell function). $fpath should not be empty for this to work.
for func in $^fpath/*(N-.x:t); autoload $func


#################################### options ####################################
setopt  EXTENDEDGLOB     # file globbing is awesome
setopt  AUTOCD           # jump to the directory.
setopt  NO_BEEP          # self explanatory
setopt  COMPLETE_IN_WORD #allow tab completion in the middle of a word
setopt  ALWAYS_TO_END    # Push that cursor on completions.
setopt  AUTOMENU         # Tab-completion should cycle.
setopt  AUTOLIST         # ... and list the possibilities.
setopt  AUTO_PARAM_SLASH # Make directories pretty.
setopt  AUTO_NAME_DIRS   # change directories  to variable names
setopt  GLOB_DOTS        # . not required for correction/completion
setopt  CLOBBER         # Allows  `>'  redirection to truncate existing files, and `>>' to create files

setopt   LONG_LIST_JOBS AUTO_RESUME AUTO_CONTINUE NOTIFY #jobs

#setopt nocorrect nocorrectall correct correctall
setopt   REC_EXACT RC_QUOTES CDABLE_VARS
setopt   AUTO_PUSHD PUSHD_MINUS PUSHD_TO_HOME

#################################### limits ####################################
unlimit
limit stack 8192
limit core 0
limit -s
umask 022


#################################### kbd ####################################
#x-xterm, s-screen, l-linux

#^A, ^E  +xsl
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line

#Home, End +x
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
#Home, End, +s
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line
#Home, End -
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line

#^B, Delete +xsl
bindkey "^B" delete-char
bindkey "^[[3~" delete-char

#ctrl+<- +x
bindkey "^[[1;5D" backward-word
#bindkey "^[[C" forward-word

bindkey "^o" beep
# history search
bindkey "^R" history-incremental-search-backward
bindkey "^S" history-incremental-search-forward

#################################### Completion Styles ####################################

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate 


# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/5 )) numeric )'


# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions


# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''


# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'


# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters


# command for process lists, the local web server details and host completion
zstyle ':completion:*:processes' command 'ps -o pid,s,nice,stime,args'
zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
zstyle '*' hosts $hosts


# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
   '*?.old' '*?.pro'
# the same for old style completion
#fignore=(.o .c~ .old .pro)


# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'


zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'
zstyle ':completion:*:expand:*' tag-order all-expansions
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
zstyle '*' hosts $hosts
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' '*?.old' '*?.pro'
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*' max-errors 2
zstyle :compinstall filename '.zshrc'
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*' completer _complete _list _oldlist _expand _ignored _match _correct _approximate _prefix
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' add-space true

zstyle ':completion:*:processes' command 'ps -xuf'
zstyle ':completion:*:processes' sort false
zstyle ':completion:*:processes-names' command 'ps xho command'

zstyle ':completion:*:cd:*' ignore-parents parent pwd
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $((($#PREFIX+$#SUFFIX)/3 )) numeric )'
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' menu select=long-list select=0
zstyle ':completion:*' old-menu false
zstyle ':completion:*' original true
zstyle ':completion:*' substitute 1
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' verbose true
zstyle ':completion:*' word true

# Install or start ohmyzsh
if [ ! -f "$ZSH/oh-my-zsh.sh" ] ; then 
    export KEEP_ZSHRC=yes
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/${TRUSTED_OH_MY_REVISION}/tools/install.sh)" ;
else
    source $ZSH/oh-my-zsh.sh
fi

# Tune RPROMPT
RPROMPT="${return_code}$(virtualenv_prompt_info) $my_gray%n@%m%{$reset_color%} %F{cyan}%B%*%b%f%"

# Load local zsh config
[ -f ${HOME}/.zshlocal ] && source ${HOME}/.zshlocal

# INFO: if STARTING_PATH is set for non-root then change directory
# But this can be skipped if IGNORE_STARTING_PATH=true.
# HINT: PyCharm already changes CWD for wsl terminals, so you might want to pass IGNORE_STARTING_PATH to wsl,
# to do this: go to settings/Tools/Terminal and set env variables to: 
# IGNORE_STARTING_PATH=true;WSLENV=IGNORE_STARTING_PATH
if [ $UID = 0 ] || [ -n "$STARTING_PATH" ] && [ "$IGNORE_STARTING_PATH" != true ] ; then
    cd "$STARTING_PATH"
    unset STARTING_PATH
fi
