#!/bin/bash
FONT="-*-liberation sans-medium-r-normal-*-96-*-*-*-*-*-iso10646-1" # шрифт сообщения
export DISPLAY=:0.0 # дисплей (можно узнать набрав в иксах в консоли 'echo $DISPLAYE')
SOUDNFILE=""
#SOUNDFILE="/usr/share/games/abuse/sfx/pldeth02.wav" # звук который будет проигранпри срабатывании
#SOUNDFILE="/usr/share/games/abuse/sfx/telept01.wav"
SOUNDREPEATNUM=1
OSD_TEXT="Allarm triggered!" # сообщение "по умолчанию"
COLOR="#99aaa0" # цвет сообщения
DISP_TIME=7 # время отображения сообщения в секундах

if [[ ! -e /usr/bin/play ]] || [[ ! -e /usr/bin/mplayer ]]; then
    echo 'No play or mplayer found! Please install one of them' 1>&2
    exit 1;
fi

if [[ $1 == "--set" ]]; then
    SELF=$0
    TIME=$2
    shift 2
    TEXTO=$@
    at "$TIME" << EOF
$SELF $TIME! $TEXTO
EOF
    exit 0;
fi

if ! [ -t 0 ]; then
    PIPED=`/bin/cat -`
fi

if [ -n "$PIPED" ]; then
    OSD_TEXT="$PIPED"
elif [[ $* ]]; then
    OSD_TEXT="$*"
fi

notify-send "$OSD_TEXT"

HOUR=`/bin/date +%k`
if [ $HOUR -ge 9 ] && [ $HOUR -lt 18 ] ; then
    if [[ -e /usr/bin/play ]]; then
	for i in $( seq $SOUNDREPEATNUM ) ; do 
	    /usr/bin/play $SOUNDFILE >>/dev/null 2>&1;
	done
    elif [[ -e /usr/bin/mplayer ]];then
	for i in $( seq $SOUNDREPEATNUM ) ; do 
	    /usr/bin/mplayer $SOUNDFILE >>/dev/null 2>&1;
	done
    fi
fi
