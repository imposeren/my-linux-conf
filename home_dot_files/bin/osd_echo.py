#!/usr/bin/python
import pyosd
import time
import sys
from Xlib.display import Display
DEFAULT_FONT="-*-liberation sans-medium-r-normal-*-36-*-*-*-*-*-iso10646-1"
current = Display().screen()

font=DEFAULT_FONT
font_size=int(font.split("-")[7])
space_size = font_size/3.6

width = current.width_in_pixels
height = current.height_in_pixels
delay = 3
def center(string):
    return " "*int(width/2/space_size- len(string))+string

strings = (" ".join(sys.argv[1:])).split("\n")
lines= len(strings)
middle = int((height-font_size*lines)/2)
p = pyosd.osd(font=font,
              offset=middle,colour="#11EE11",shadow=2,timeout=delay,lines=lines)
for i,string in enumerate(strings):
    print string
    p.display(center(string),line=i)
time.sleep(3)

