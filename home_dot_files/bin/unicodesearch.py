#!/usr/bin/env python
from __future__ import print_function
from sys import argv, version_info, stdout
import csv
import re
import codecs
import locale
if version_info[0] == 3:
    unichr = chr

data = '/usr/share/misc/unicode'
descriptions = csv.reader(open(data), delimiter=';')
request = re.compile(" ".join(argv[1:]), flags=re.I)

stdout = codecs.getwriter(stdout.encoding)(stdout)
for record in descriptions:
    if request.findall(record[1]):
        (code, descr) = record[:2]
        stdout.write(code+" "+unichr(int(code, 16))+" "+descr+"\n")
