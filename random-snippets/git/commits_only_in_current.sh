#!/bin/bash

#
# find all commits in current branch that are not in branches listed as arguments of this script
#
#
# Usecases:
#
# * check if current "super-branch" has commits only from limited list of sub-feature branches.
#
# Example:
#
# git checkout complex-feature
# ${THIS_SCRIPT} master correct-sub-feature1 correct-sub-feature2
#

TASKS_REGEXP=`echo $@ | cut -d ' ' --output-delimiter='|' -f 1-`

git pull -q

set -e
git fetch -q origin
    
MATCHED_BRANCHES=`git branch -a | grep -E ${TASKS_REGEXP} | sed 's/ //g'`
if [[ -n "${MATCHED_BRANCHES}" ]] ; then
    echo "^"${MATCHED_BRANCHES} "--" | cut -d$'\n' --output-delimiter=' ^' -f 1- | \
    xargs -- git --no-pager log --no-merges --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit feature-scheduler "^Steven" || true
    echo "\n\n"
fi

set +e
