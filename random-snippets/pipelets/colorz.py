# -*- coding: utf-8 -*-
"""
Created on Sun Nov 20 22:13:59 2011

@author: Imposeren
"""
from __future__ import division, print_function
import cython
from colorsys import rgb_to_hsv as rgb_to_hsv0


def rgb_to_hsv(color):
    """convert web color to hue
    '#0060ff' -> (0.6, 1.0, 1.0)
    """
    return rgb_to_hsv0(int(color[1:3], 16)/255,
                       int(color[3:5], 16)/255,
                       int(color[5:7], 16)/255)

@cython.locals(val=cython.double, start=cython.double, end=cython.double,
               delta=cython.double)
def cycled(val, start, end):
    delta = end - start
    if start <= val <= end:
        return val
    else:
        return val - val // delta * delta + start

@cython.locals(val=cython.double)
def hue(val):
    """limit *val* to be in range 0..1
    If *val* is out of range add or substract 1 until it becomes in range
    P.S. It's just special case *cycle* function

    """
    if 0 <= val <= 1:
        return val
    else:
        return val - val // 1

@cython.locals(hue2=cython.double, hue1=cython.double, period=cython.double)
def hue_diff(hue2, hue1, direction="CW", period=1):
    """Distance between *hue2* and *hue1* in *direction* (CW or CCW).
    hue have cycle of 1
    """
    if direction == "CW":
        if hue2 > hue1:
            return hue2 - hue1
        else:
            return period - (hue1 - hue2)
    else:#CCW
        if hue1 > hue2:
            return hue1 - hue2
        else:
            return period - (hue2 - hue1)