from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("colorz",
                         ["colorz.py"],
                         extra_compile_args=["-O3", "-march=native"],
#                         extra_compile_args=["-O3", "-march=native", '-fopenmp'],
#                         extra_link_args=['-fopenmp'],
                        ),
              ]

setup(
      name = 'Misc code',
      cmdclass = {'build_ext': build_ext},
      ext_modules = ext_modules
)
