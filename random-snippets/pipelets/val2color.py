#!/usr/bin/env python
"""
Return color that is mapped to value.
Usage:
    val2color.py startval endval startcolor endcolor targetval direction
    return color that is mapped to *targetval*
    *startcolor* is mapped to *startval*
    *endcolor* is mapped to *endval*
    all ather mappings are computed
    colors are changed in CW or CCW *direction*

"""
from __future__ import division, print_function
from colorz import rgb_to_hsv, hue, hue_diff
from colorsys import hsv_to_rgb

if __name__ == "__main__":
    import sys
    startval, endval, startcolor, endcolor, targetval, direction = sys.argv[1:7]
    startval, endval, targetval = (float(startval), float(endval),
                                   float(targetval))

    if targetval <= startval:
        print(startcolor)
        sys.exit(0)
    elif targetval >= endval:
        print(endcolor)
        sys.exit(0)

    H1, S1, V1 = rgb_to_hsv(startcolor)
    H2, S2, V2 = rgb_to_hsv(endcolor)

    mod_val = (targetval - startval) / (endval - startval)
    if direction == "CW":
        H3 = hue(H1 + hue_diff(H2, H1) * mod_val)
    elif direction == "CCW":
        H3 = hue(H1 - hue_diff(H2, H1, "CCW") * mod_val)
    else:
        print("#000000")
        sys.exit(1)

    if S2 > S1:
        S3 = S1 + abs(S2 - S1) * mod_val
    else:
        S3 = S1 - abs(S2 - S1) * mod_val
    if V2 > V1:
        V3 = V1 + abs(V2 - V1) * mod_val
    else:
        V3 = V1 - abs(V2 - V1) * mod_val

    R, G, B = [x*255 for x in hsv_to_rgb(H3, S3, V3)]

    print("#%02X%02X%02X" % (R, G, B))
