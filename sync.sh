#!/bin/sh
SCRIPT_PATH=`readlink -e $0`
REPO=`dirname ${SCRIPT_PATH}`
RSYNC_OPTS="--archive --hard-links --delete --progress -i --ignore-errors --delete-excluded --exclude='*.pyc' --exclude='*.cache'"

echo Syncing home dotfiles
rsync --archive --list-only "${REPO}/home_dot_files" | awk -F "dot_files/" '/1/ {print $2}' > /dev/shm/filelist
rsync ${RSYNC_OPTS} --existing --files-from=/dev/shm/filelist "${HOME}/" "${REPO}/home_dot_files"
rsync ${RSYNC_OPTS} --existing "${HOME}/.local" "${REPO}/home_dot_files"
rsync ${RSYNC_OPTS} --existing "${HOME}/.config" "${REPO}/home_dot_files"
rsync ${RSYNC_OPTS} "${HOME}/bin/" "${REPO}/home_dot_files/bin/"

echo Syncing /etc
rsync --archive --list-only "${REPO}/etc" | awk -F "etc/" '/1/ {print $2}' > /dev/shm/filelist
rsync ${RSYNC_OPTS} --existing --files-from=/dev/shm/filelist "/etc/" "${REPO}/etc/"

echo syncing ST3
TARGET=".config/sublime-text-3/Packages/User"
rm -rf "${REPO}/home_dot_files/${TARGET}" 2>>/dev/null || true
rsync ${RSYNC_OPTS} -L "${HOME}/${TARGET}/" "${REPO}/home_dot_files/${TARGET}/"
rm $REPO/home_dot_files/.config/sublime-text-3/Packages/User/Preferences.sublime-settings.*.bak 2>>/dev/null || true

crontab -l|sed "s/-p '.*'/-p 'secret'/" > $REPO/user_crontab
grep -Ev '^password' /etc/msmtprc > $REPO/etc/msmtprc
apt-mark showmanual | sort -u > $REPO/apt-manual-installed
dconf dump /org/gnome/desktop/ >$REPO/dconf_org_gnome_desktop.dump

# rm /dev/shm/filelist
