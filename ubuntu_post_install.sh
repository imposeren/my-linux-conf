#!/bin/bash
# NOTE: this script contains some `read` commands that will prompt you for input.
# if you'd like to have automated install then comment `read`s and `if/fi`s after them

read -p "If it's a bootsrapped system then kernel locales and similar software should be installed. Is this a bootstrapped system? " -n 1 -r
echo
if [[ $REPLY =~ ^[yY]$ ]] ; then
	echo "Configuring /etc/apt/sources.list..."
	echo "
###### Ubuntu Main Repos
deb http://ubuntu.org.ua/ubuntu/ saucy main restricted universe multiverse

###### Ubuntu Update Repos
deb http://ubuntu.org.ua/ubuntu/ saucy-security main restricted universe multiverse
deb http://ubuntu.org.ua/ubuntu/ saucy-updates main restricted universe multiverse

###### Ubuntu Partner Repo
deb http://archive.canonical.com/ubuntu saucy partner
" > /etc/apt/sources.list


	echo "Take part in popularity-contest..."
	apt-get -y install  popularity-contest

	echo "Setting up russian and english UTF-8 locales..."
	apt-get -y install locales
	locale-gen ru_RU.UTF-8
	locale-gen en_US.UTF-8
	# set default locale to russian:
	echo 'LANG="ru_RU"' >> /etc/default/locale
	echo 'LC_ALL="ru_RU.UTF-8"' >> /etc/default/locale
	dpkg-reconfigure locales
	apt-get -y install console-cyrillic
	
	echo "Installing kernel..."
	apt-get -y install linux-image-generic linux-headers-generic

	# Other lazy to comment software
	apt-get -y install software-properties-common samba4 cups locate openssh-server command-not-found lubuntu-software-center 
fi

echo
read -p "Should lxqt be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	add-apt-repository -y -u ppa:mati75/qt5ct
	apt-get -y install openbox lxqt lxappearance lxappearance-obconf gtk-chtheme gtk-theme-config gtk-theme-switch obconf-qt chameleon-cursor-theme qt5ct qt4-qtconfig dconf-tools gconf-editor gnome-settings-daemon crudini compton
	apt-get -y install fonts-hack-ttf fonts-hack-otf fonts-noto-hinted arandr
	apt-get -y install breeze-icon-theme gtk3-engines-breeze kde-style-breeze kde-style-breeze-qt4
fi

echo
read -p "Should wicd be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install wicd
fi

echo
read -p "Should pulseaudio equalizer be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	add-apt-repository -y -u ppa:nilarimogard/webupd8
	apt-get -y install pulseaudio-equalizer
fi


echo
echo "Installing libreoffice..."
apt-get -y install libreoffice libreoffice-l10n-ru libreoffice-l10n-uk

echo
echo "Installing urxvt for terminal and tmux for terminal multiplexing"
apt-get -y install rxvt-unicode tmux

echo
echo "Installing resources monitoring software..."
apt-get -y install iotop htop atop lsof

echo
echo "Installing Torrent downloading soft..."
apt-get -y install transmission-qt

echo
echo "Installing image viewer and document viewer..."
apt-get -y install viewnior evince

echo
read -p "Should xfburn (cd mastering and burning software) be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install xfburn
fi

echo
echo "Installing image editing software..."
apt-get -y install gimp inkscape mypaint mypaint-data-extras

echo
read -p "Should minidlna be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install minidlna
fi

echo
read -p "Should mpd be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install mpd ario
fi

echo
echo "Instaling local caching dns server..."
apt-get -y install dnsmasq

echo
echo "Soft for screenshot taking and global keys binding..."
apt-get -y install scrot xbindkeys

echo
echo "Installing digital camera management soft..."
apt-get -y install gphoto2 gphotofs

echo
echo "Panorama stitching..."
apt-get -y install hugin

echo
echo "Guitar tuning..."
apt-get -y install lingot

echo
echo "Video editing..."
apt-get -y install avidemux

echo
echo "Video players..."
apt-get -y install smplayer vlc

echo
echo "Disk health monitoring..."
apt-get -y install smartmontools mailutils gsmartcontrol
# enable monitoring
sed -i 's/#start_smartd=yes/start_smartd=yes/' /etc/default/smartmontools
# configure alias for root so that you will receive mails on your regular mail account
read -p "Enter your main email address to receive notifications: " MAILTO
echo
echo "root:   $MAILTO" > /etc/aliases
newaliases

## CPU frequency controlling (for desktop only!)
## apt-get install -y cpufrequtils
## I'm on desktop so set to maximum cpu to maximum
# echo 'GOVERNOR="performance"' >> /etc/default/cpufrequtils

## install TLP (only for laptops!)
echo
read -p "Should TLP be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install tlp
	systemctl enable tlp.servic
	systemctl disable ondemand || true
fi

echo
echo "Installing network-related tools"
apt-get -y install mtr dnsutils nmap

echo
echo "Installing fonts"
apt-mark manual fonts-droid
apt-get -y install fonts-oldstandard

echo
echo "Installing development and documentation editing soft"
apt-get -y install python-virtualenv virtualenvwrapper
# rst editor and converter:
apt-get -y install retext rst2pdf

echo
echo "Installing (D)VCS:"
apt-get -y install mercurial git

echo
read -p "Should DB-related and server-related software for development be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install postgresql sqliteman mysql-server sqlite3 python-mysqldb python-psycopg2 python-sqlite redis-server rabbitmq-server
	apt-get -y install uwsgi-plugin-python nginx-full
	# misc dev requirements
	apt-get -y install libxml2-dev libxslt-dev python-dev libsqlite3-dev libjpeg-dev yui-compressor python-mysqldb build-essential libmysqlclient-dev postgresql-server-dev-all libz-dev libreadline-dev libssl-dev liblzma-dev libjpeg-dev libpng12-dev python-imaging libbz2-dev
fi


# 2-panel file manager
echo 
echo "Installing spacefm"
apt-get -y install spacefm

# keyboard laoyt switching configuration and indication
echo
read -p "Should qxkb be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install qxkb
fi

echo
read -p "Should xxkb be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install xxkb
fi

echo
read -p "Install wine (including 32bit arch)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	wget -q -O - https://dl.winehq.org/wine-builds/Release.key | apt-key add -
	apt-add-repository -y -u 'https://dl.winehq.org/wine-builds/ubuntu/'

	dpkg --add-architecture i386
	apt-get -y install wine-stable

	# set wine to be 32 bit by default
	echo
	read -p "Should 32bit version of wine be used by default? " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]] ; then
		mkdir /etc/profile.d
		echo 'export WINEARCH=win32' >> /etc/profile.d/wine.sh
		chmod +x /etc/profile.d/wine.sh
	fi
fi

echo
read -p "Install google-chrome-stable? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
	sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
	apt-get install google-chrome-stable
fi

echo
read -p "Should firefox be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	echo "Installing firefox..."
	apt-get -y install firefox
fi

echo
read -p "Should valve's steam be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	wget http://media.steampowered.com/client/installer/steam.deb
	gdebi --n steam.deb
fi

echo
read -p "Should we add deadsnakes repo to allow multiple python version installation? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	add-apt-repository -y -u ppa:deadsnakes/ppa
fi

echo
read -p "Should lightdm be used? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	echo "To set greeter session to razor: `greeter-session=lightdm-razor-greeter`. Press enter key to start editing or specify editor (defalt nano)."
	read EDITOR
	EDITOR=${EDITOR:-nano}
	$EDITOR /etc/lightdm/lightdm.conf
	dpkg-reconfigure lightdm  
fi

echo
read -p "Should grub be reconfigured? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	# update grub
	echo "Enter path to main hdd (default /dev/sda): "
	read DEVICE_PATH
	DEVICE_PATH=${DEVICE_PATH:-/dev/sda}
	update-grub $DEVICE_PATH
fi

# Other lazy to comment software
echo
echo "Installing some other soft"
apt-get -y install vim numlockx autocutsel wmname xsel xdotool xclip pwgen zsh lshw zip convmv dos2unix jstest-gtk gtk-recordmydesktop 

echo
read -p "Should vsftpd be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install vsftpd
fi

echo
read -p "Should gcdemu and iat be installed? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	add-apt-repository -y -u ppa:cdemu/ppa
	apt-get -y install gcdemu iat
fi

echo
read -p "Install mysql and rabbitmq? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	apt-get -y install mysql-server rabbitmq-server
fi

echo
read -p "Should new user be created? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] ; then
	echo "Enter username (default: simpleuser)"
	read USERNAME
	USERNAME=${USERNAME:-simpleuser}
	useradd -m -G sudo,mail,audio $USERNAME
	passwd $USERNAME
fi

